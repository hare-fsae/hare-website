---
layout: post
title: Server Down
show_tile: false
---

**The server is currently up!**

Yay! Keep doing good work!

<!--- **ETA:** Unknown
- **Reason:** Hosting location became unavailable
- **Solution in progress:** See below-->

───────────────

*6th July 2023*

Y'all,

I messed up. I actually knew this was coming ahead of time, but life was busy and I never got around to coming up with a good plan much less post an announcement about it. I was sort of counting on something that wasn't a guarantee, perhaps even improbable. I want to apologize, and I can definitely say I've learned an important lesson as a system administrator: plan ahead.

Tomorrow (Friday, July 7th) I'll explain what happened at the meeting, and we'll discuss the options and **vote on it. This will include the new PDM server stuff**, since the plan was to also host it on this same server once I got the environment set up.

In the meantime **if you need files that are on the server**, I will work on getting it set up on a little temporary network in the club room so that you can come in and get stuff off. I'm hoping to have that ready by tomorrow's meeting. Nextcloud Talk is also down, so reach out to my (below) or Austin's UCCS email if you need anything.

Again, I'm sorry,

Josh L - *(aka "agent" Josh)*

[jlangle2@uccs.edu](mailto:jlangle2@uccs.edu)
