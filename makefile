
make: clean
	@echo

	# Building website
	bundler exec jekyll build -d public
	@echo

serve:
	@echo
	# Launching website preview
	# Kill and restart this if you modify _config.yml
	bundler exec jekyll serve --livereload
	@echo

.PHONY: clean install-deps

clean:
	@echo
	# Clean previous builds
	bundler exec jekyll clean
	@echo

install-deps:
	@echo
	# Install dependencies
	bundler install
