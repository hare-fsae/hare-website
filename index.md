---
layout: home
title: Home
landing-title: 'High Altitude Race Engineering'
description: null
image: null
author: null
show_tile: false
---

<section id="two">
    <div class="inner">
        <header class="major">
            <h2>Formula SAE</h2>
        </header>
        <p>Formula SAE is a competition in which colligate teams can compete to engineer the formula race car. Teams must pass inspections to compete in various events that test the acceleration, maneuverability, endurance and fuel economy performance of the car. These are known as Dynamic Events. There are also Static Events which assess the team's overall design and business side.</p>
        <ul class="actions">
            <li><a href="https://fsaeonline.com/" target="_blank" class="button next">FSAE Website</a></li>
            <li><a href="https://www.youtube.com/watch?v=wQiR2orw3J4" target="_blank" class="button">Purdue Overview Video</a></li>
            <li><a href="https://www.youtube.com/watch?v=NVU2HQNMv8Y" target="_blank" class="button">CashedOutCars Video</a></li>
        </ul>
    </div>
    <div class="inner">
        <header class="major">
            <h2>Donate Now</h2>
        </header>
        <p>Support engineering students gain experience and receive a tax break via tax form 501(c)(3).</p>
        <ul class="actions">
            <li><a href="https://giving.cu.edu/fund/uccs-formula-society-automotive-engineers-fund" target="_blank" class="button">Donate</a></li>
        </ul>
    </div>
    <div class="inner">
        <header class="major">
            <h2>There's a place for everyone</h2>
        </header>
        <p>We are recruiting for all roles!</p>
        <div style="display:flex; flex-flow:row wrap; justify-content:center;">
            <div style="margin:1em; flex-basis: 30%; min-width: 200px;">
                <h4>Mechanical</h4>
                <p><b>Our mechanical team designs and manufactures the mechanical systems on the car.</b> Including the chassis, suspension, control, and battery housing. They work with 3D CAD programs such as Solidworks to design a full digital model used for manufacture. The mechanical team is also responsible for welding, fastening, and integration. We are constantly improving our methods, aerodynamics, traction and more. Gain experience designing and manufacturing mechanical systems!</p>
            </div>
            <div style="margin:1em; flex-basis: 30%; min-width: 200px;">
                <h4>Electrical</h4>
                <p><b>The electrical team designs, programs, and implements all the electrical circuit boards and communications within the car.</b> Building an electric formula car involves designing Printed Circuit Boards and programming firmware as well as systems integration. The electrical team optimizes the electrical efficiency of the systems and ensures their safety, accuracy, and robustness . Gain experience developing electrical and software systems!</p>
            </div>
            <div style="margin:1em; flex-basis: 30%; min-width: 200px;">
                <h4>Business</h4>
                <p><b>The business and marketing team manages timelines, budgets, and communication.</b> The goals are to design a business plan, gain sponsors, and maintain schedules. Gain experience managing and overseeing the manufacture of an electric car.</p>
            </div>
        </div>
    </div>
    <div class="inner">
        <header class="major">
            <h2>Get practical engineering skills</h2>
        </header>
        <p>Team members often find they learn more practical skills for the industry in this club than they do in their classes.</p>
        <div style="display:flex; flex-flow:row wrap; justify-content:center;">
            <div style="margin:1em; flex-basis: 30%; min-width: 200px;">
                <h4>Tools</h4>
                <p>We utilize computer assisted design (CAD) tools such as Solidworks and KiCAD to model the mechanical and electrical systems. We utilize programming languages C and Python to create firmware and software. We manufacture the car with a variety of machining techniques including CNC maching lathes. TIG welding is used to assemble the frame. </p>
            </div>
            <div style="margin:1em; flex-basis: 30%; min-width: 200px;">
                <h4>Design Principles</h4>
                <p>Improve your design process by designing for first principles, designing for manufacturing and designing for integration.</p>
            </div>
            <div style="margin:1em; flex-basis: 30%; min-width: 200px;">
                <h4>Soft Skills</h4>
                <p>Work in a multi-disciplinary team and communicate effectively to integrate a wide variety of systems into one car.</p>
            </div>
        </div>
    </div>
</section>
